# git-histedit

Inspired by Mercurial's [histedit extension](https://www.mercurial-scm.org/wiki/HisteditExtension),
*git-histedit* is a Git script that can automate the selection of the commit to run
`git rebase --interactive` on.

It is recommended to install it alongside [Git Interactive Rebase Tool](https://github.com/MitMaro/git-interactive-rebase-tool).
This extension adds a terminal user interface as an editor for `git rebase --interactive`.

## Usage

1. Add the script somewhere in your path:
	```bash
	mkdir -p ~/.local/bin
	ln -srv git-histedit ~/.local/bin/
	export PATH="${HOME}/.local/bin:${PATH}"
	```
2. Add the command in a Git alias in `~/.gitconfig`:
	```ini
	[alias]
	histedit = !git-histedit
	```
3. Run it:
	```bash
	git histedit
	```
4. Or specify a parent commit manually:
	```bash
	git histedit .^^^
	```

## License

See [COPYING](COPYING).
